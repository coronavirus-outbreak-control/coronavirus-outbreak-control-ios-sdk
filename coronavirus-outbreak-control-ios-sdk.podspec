Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '12.0'
s.name = "coronavirus-outbreak-control-ios-sdk"
s.summary = "Covid Community Alert is a worldwide open-source standard proposed by a global community to provide a service that allows the monitoring of interactions among devices in a totally anonymous way."
s.requires_arc = true

# 2
s.version = "0.1.0"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4 - Replace with your name and e-mail address
s.author = { "Rodrigo Bueno Tomiosso" => "rodrigobt20@gmail.com" }

# 5 - Replace this URL with your own GitHub page's URL (from the address bar)
s.homepage = "https://coronavirus-outbreak-control.github.io/web/"

# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source = { :git => "https://gitlab.com/coronavirus-outbreak-control/coronavirus-outbreak-control-ios-sdk.git", 
             :tag => "#{s.version}" }

# 7
s.framework = "UIKit"
s.dependency 'ReCaptcha/RxSwift'
s.dependency "Alamofire", '5.0.5'
s.dependency "Sentry", '4.5.0'


# 8
s.source_files = "coronavirus-outbreak-control-ios-sdk/**/*.{swift,xcdatamodeld}"

# 9
s.resource_bundles = {'coronavirus-outbreak-control-ios-sdk' => ['coronavirus-outbreak-control-ios-sdk/*.xcdatamodeld']}

# 10
s.resources = "coronavirus-outbreak-control-ios-sdk/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}", "coronavirus-outbreak-control-ios-sdk/*.xcdatamodeld"

# 11
s.swift_version = "4.2"

end
