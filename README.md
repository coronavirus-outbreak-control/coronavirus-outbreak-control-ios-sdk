# Coronavirus Outbreak Control iOS - Users sdk

Covid Community Alert is a worldwide open-source standard proposed by a global community to provide a service that allows the monitoring of interactions among devices in a totally anonymous way. We can notify people at risk by sending them a clear set of instructions. This will reduce the number of infected patients and allow them to receive their treatments as soon as possible.

## Installation

1. [Cocoapods](http://cocoapods.org "Cocoapods") `pod "coronavirus-outbreak-control-ios-sdk", :git => 'https://gitlab.com/coronavirus-outbreak-control/coronavirus-outbreak-control-ios-sdk.git', :branch => 'master'`

2. Run pod install

3. `Import coronavirus_outbreak_control_ios_sdk` at your AppDelegate

4. Create an instace of the sdk: `let coronaOutbreakControl = CoronavirusOutbreakControl()` at your AppDelegate

5. Add Location and Bluetooth permission description at your Info.plist

`<key>NSBluetoothAlwaysUsageDescription</key>`
`<string>To anonymously monitor your interactions we will ask you permission to use the Bluetooth.</string>`
`<key>NSBluetoothPeripheralUsageDescription</key>`
`<string>To anonymously monitor your interactions we will ask you permission to use the Bluetooth.</string>`
`<key>NSLocationAlwaysAndWhenInUseUsageDescription</key>`
`<string>To anonymously monitor your interactions we will ask you permission to use location.</string>`
`<key>NSLocationWhenInUseUsageDescription</key>`
`<string>To anonymously monitor your interactions we will ask you permission to use location.</string>`

6.  Forward your AppDelegate event calls to the SDK:

         
         func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
             coronaOutbreakControl.application(application, performFetchWithCompletionHandler: completionHandler)
         }     

         func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
             coronaOutbreakControl.application(application, willFinishLaunchingWithOptions: launchOptions)
         }     

         func applicationWillResignActive(_ application: UIApplication) {
             coronaOutbreakControl.applicationWillResignActive(application)
         }     

         func applicationDidEnterBackground(_ application: UIApplication) {
             coronaOutbreakControl.applicationDidEnterBackground(application)
         }     

         func applicationWillEnterForeground(_ application: UIApplication) {
             coronaOutbreakControl.applicationWillEnterForeground(application)
         }     

         func applicationDidBecomeActive(_ application: UIApplication) {
             coronaOutbreakControl.applicationDidBecomeActive(application)
         }     

         func applicationWillTerminate(_ application: UIApplication) {
             coronaOutbreakControl.applicationWillTerminate(application)
         }     

         func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
             coronaOutbreakControl.application(application, didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)
         }
         func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
             coronaOutbreakControl.application(application, didReceiveRemoteNotification: userInfo)
         }     

         func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
             coronaOutbreakControl.application(application, didReceiveRemoteNotification: userInfo, fetchCompletionHandler: completionHandler)
         }     

     }

7. Make a handshake on your favorite view controller. `AppDelegate.shared.coronaOutbreakControl.handshake(on: self)`

**You are done!**

The contact tracing will start once bluetooth, location (always) and notification permission are granted to the application.

Your app will receive a notification if your user had a contact with a suspicious device.

You can look our [DEMO PROJECT HERE](https://gitlab.com/coronavirus-outbreak-control/coronavirus-outbreak-control-ios-users-sdk-demo "DEMO PROJECT HERE") 

##Documentaion

Here there are explained exposed methods and variables inside the SDK.

###var canRun: Bool

Only readable value, it asserts if the SDK has permissions to start the contact tracing, in particular he looks for the following permissions:

- Bluetooth: *granted*
- Location: granted *always* permission
- Notification: *granted*

###var isRunning: Bool

Only readable value, it asserts if the contact tracing engine is running or not.

###func start() -> Bool

Ask the SDK to start the contact tracing engine, if the engine has not enough permissions it won't start.

The functions return if the contact tracing engine has been started or not.

Multiple called to the method doesn't affect the contact tracing engine.

###func stop()

This method stops the contact tracing engine.

###func setShareLocation(_ share : Bool)

Specify the will of the user to share a sporadic location in order to help the research. The location collection is not performed realtime and are sent with a precision of hundres of meters.

Default value is false.

###func getShareLocation() -> Bool

Return the preference of the user to share sporadically his location during an interaction.

###func getIdentifierDevice() -> Int?

The method returns the device id if the handhsake has been performed and a device id has been registered.

###func requestPermission(requests: [CoronavirusOutbreakControlPermission] = [.bluetooth, .locationAlways, .notification])

The method exposes the possibility to ask permissions of bluetooth, location and notification to the user.

The method can be called with a single premission request.

###func permissionIsGranted(permission: CoronavirusOutbreakControlPermission) -> Bool

The method informs if a specific permision has been granted or not.

##Permission Status

###func getBluetoothPermissionStatus() -> BluetoothManager.Status

Return the status of bluetooth permission.

###func getLocationPermissionStatus() -> LocationManager.AuthorizationStatus

Return the status of location permission.

###func getNotificationPermissionStatus() -> NotificationManager.PermissionStatus?

Return the status of notification permission.

##Notification Events

You can observe broadcast notification by registering the to specific events generated by the SDK in the following way: `NotificationCenter.default.addObserver(self, selector: Selector, name: NSNotification.Name(<EVENT_NAME>), object: nil)`.
These events are generated by the change of the status of the permission.

###Bluetooth

You can get the event name from the method: `func getBluetoothPermissionChangeEvent() -> String`, this method will bring an object of type `BluetoothManager.Status` described in the previous section.

###Location

You can get the event name from the method: `func getLocationPermissionChangeEvent() -> String`, this method will bring an object of type `LocationManager.AuthorizationStatus` described in the previous section.

###Push Notifications

You can get the event name from the method: `func getNotificationPermissionChangeEvent() -> String`, this method will bring an object of type `NotificationManager.PermissionStatus` described in the previous section.