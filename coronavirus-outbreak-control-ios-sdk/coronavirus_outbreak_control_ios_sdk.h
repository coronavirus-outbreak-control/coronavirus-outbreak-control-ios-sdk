//
//  coronavirus_outbreak_control_ios_sdk.h
//  coronavirus-outbreak-control-ios-sdk
//
//  Created by Rodrigo Bueno Tomiosso on 23/04/20.
//  Copyright © 2020 mourodrigo. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for coronavirus_outbreak_control_ios_sdk.
FOUNDATION_EXPORT double coronavirus_outbreak_control_ios_sdkVersionNumber;

//! Project version string for coronavirus_outbreak_control_ios_sdk.
FOUNDATION_EXPORT const unsigned char coronavirus_outbreak_control_ios_sdkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <coronavirus_outbreak_control_ios_sdk/PublicHeader.h>


