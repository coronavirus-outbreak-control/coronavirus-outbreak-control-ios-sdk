//
//  LocationManager.swift
//  Coronavirus Herd Immunity
//
//  Created by Antonio Romano on 02/03/2020.
//  Copyright © 2020 Coronavirus-Herd-Immunity. All rights reserved.
//

import Foundation
import CoreLocation

// https://www.raywenderlich.com/5247-core-location-tutorial-for-ios-tracking-visited-locations

public class LocationManager : NSObject, CLLocationManagerDelegate{

    public enum AuthorizationStatus {
        case allowedAlways, allowedWhenInUse, denied, notDetermined, notAvailable
    }
    
    public static var shared = LocationManager()
    let locationManager : CLLocationManager
    
    private override init(){
        self.locationManager = CLLocationManager()
        super.init()
        locationManager.delegate = self
    }
    
    private var _isRunning = false
   
    var isRunning: Bool {
        return _isRunning
    }
    
    public func getPermissionStatus() -> AuthorizationStatus{
        switch (CLLocationManager.authorizationStatus()){
            case .authorizedAlways:
                return .allowedAlways
            case .authorizedWhenInUse:
                return .allowedWhenInUse
            case .denied:
                return .denied
            case .notDetermined:
                return .notDetermined
            case .restricted:
                return .notAvailable
            }
    }
    
    public func isServiceEnabledForApp() -> Bool{
        // if true and denied the location is disabled for app only
        return CLLocationManager.locationServicesEnabled()
    }
    
    public func requestPermission() {
        if #available(iOS 13.0, *) {
            self.locationManager.requestWhenInUseAuthorization()
        } else {
            self.locationManager.requestAlwaysAuthorization()
        }
    }
    
    public func startMonitoring() {
        LocationManager.shared.locationManager.allowsBackgroundLocationUpdates = true
        LocationManager.shared.locationManager.startMonitoringSignificantLocationChanges()
        LocationManager.shared.locationManager.startMonitoringVisits()
        _isRunning = true
    }
    
    public func stopMonitoring() {
        LocationManager.shared.locationManager.stopMonitoringVisits()
        LocationManager.shared.locationManager.stopMonitoringSignificantLocationChanges()
        _isRunning = false
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways:
            self.startMonitoring()
            NotificationCenter.default.post(name: NSNotification.Name(Constants.Notification.locationChangeStatus), object: AuthorizationStatus.allowedAlways)
            break
        case .authorizedWhenInUse:
            NotificationCenter.default.post(name: NSNotification.Name(Constants.Notification.locationChangeStatus), object: AuthorizationStatus.allowedWhenInUse)
            break
        case .denied:
            NotificationCenter.default.post(name: NSNotification.Name(Constants.Notification.locationChangeStatus), object: AuthorizationStatus.denied)
            break
        case .notDetermined, .restricted:
            NotificationCenter.default.post(name: NSNotification.Name(Constants.Notification.locationChangeStatus), object: AuthorizationStatus.notDetermined)
            break
        }
    }
    
    public func getLocationAndUpdate() -> CLLocation?{
        if self.getPermissionStatus() != .allowedAlways{
            return nil
        }
        self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        return self.locationManager.location
    }
    
    func locationManager(_ manager: CLLocationManager, didVisit visit: CLVisit) {
      // create CLLocation from the coordinates of CLVisit
        BackgroundManager.backgroundOperations()
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let _ = BackgroundManager.backgroundOperations()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        Logger.error("Location manager failed: \(error.localizedDescription)")
    }
    
}
