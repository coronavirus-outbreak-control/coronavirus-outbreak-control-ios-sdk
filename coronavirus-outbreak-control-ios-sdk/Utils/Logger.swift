//
//  Logger.swift
//
//
//  Created by Rodrigo Bueno Tomiosso on 29/04/20.
//  Copyright © 2020 mourodrigo. All rights reserved.

import Foundation
import os

//************************************************
// MARK: - Logger
//************************************************

/// Just a wrapper over Apple's "os_log".
/// A `Logger` is the central type in `Logger` ecosystem.
/// Its central function is to emit log messages using one of the methods corresponding to a log level.
///
/// The most basic usage of a `Logger` is

enum Logger {
    typealias Category = OSLog

    private static func log(
        _ message: String,
        category: Logger.Category = .default,
        type: OSLogType = .default,
        file: String = #file, function: String = #function, line: Int = #line) {


        var canLog: Bool {
            switch type {
            //case .default: return true
            case .info: return true
            case .debug: return true
            //case .error: return true
            //case .fault: return true
            default:
                return false
            }
        }
        
        if canLog == false { return }
        
        #if DEBUG
            let prefix = "[CoronavirusOutbreakControl-SDK] [\(function)] [\(line)]"
            let finalMessage = "\(prefix) \(message)"

            os_log("%@", log: category, type: type, finalMessage)
        #endif
    }
}


extension Logger {

    static func `default`(
        _ message: String,
        category: Logger.Category = .default,
        file: String = #file, function: String = #function, line: Int = #line) {

        self.log(message, category: category, type: .default, file: file, function: function, line: line)
    }

    static func info( // regular notifications from the sdk to the sample app
        _ message: String,
        category: Logger.Category = .default,
        file: String = #file, function: String = #function, line: Int = #line) {

        self.log(message, category: category, type: .info, file: file, function: function, line: line)
    }

    static func debug(
        _ message: String,
        category: Logger.Category = .default,
        file: String = #file, function: String = #function, line: Int = #line) {
        #if DEBUG
        self.log(message, category: category, type: .debug, file: file, function: function, line: line)
        #endif
    }

    static func error(
        _ message: String,
        category: Logger.Category = .default,
        file: String = #file, function: String = #function, line: Int = #line) {

        self.log(message, category: category, type: .error, file: file, function: function, line: line)
    }

    static func fault(
        _ message: String,
        category: Logger.Category = .default,
        file: String = #file, function: String = #function, line: Int = #line) {

        self.log(message, category: category, type: .fault, file: file, function: function, line: line)
    }
}
