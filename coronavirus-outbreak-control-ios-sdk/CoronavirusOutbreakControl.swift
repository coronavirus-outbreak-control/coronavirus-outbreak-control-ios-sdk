//
//  AppDelegate.swift
//  Coronavirus Herd Immunity
//
//  Created by Antonio Romano and @mourodrigo on 23/02/2020.
//  Copyright © 2020 Coronavirus-Herd-Immunity. All rights reserved.
//

import UIKit
import Sentry
import BackgroundTasks
import ReCaptcha
import RxSwift

// background fetch: https://www.hackingwithswift.com/example-code/system/how-to-run-code-when-your-app-is-terminated
// https://medium.com/snowdog-labs/managing-background-tasks-with-new-task-scheduler-in-ios-13-aaabdac0d95b
// scrollview: https://fluffy.es/scrollview-storyboard-xcode-11/

public class CoronavirusOutbreakControl: UIResponder, UIApplicationDelegate {
    
    var recaptcha = try? ReCaptcha(
        apiKey: Constants.Setup.keySite,
        baseURL: URL(string: Constants.Setup.keyDomain)!
    )

    public func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
                
        // MARK: Registering Launch Handlers for Tasks
        if #available(iOS 13.0, *) {
            BGTaskScheduler.shared.register(
            forTaskWithIdentifier: Constants.Setup.backgroundPushIdentifier,
            using: DispatchQueue.global()) {
                task in
                // Downcast the parameter to an app refresh task as this identifier is used for a refresh request.
                self.handlePushInteractions(task)
            }
        } else {
            // Fallback on earlier versions
        }
        
        do {
            Client.shared = try Client(dsn: "https://2d08d421fc5e40f1ba4a04ee468b5898@sentry.io/4506990")
            try Client.shared?.startCrashHandler()
        } catch let error {
            Logger.error("Client.shared?.startCrashHandler() \(error.localizedDescription)")
        }
        
        autoStart()
        
        return true
    }
    
    @available(iOS 13.0, *)
    private func schedulePushInteractions() {
        BGTaskScheduler.shared.cancelAllTaskRequests()
        let request = BGAppRefreshTaskRequest(identifier: Constants.Setup.backgroundPushIdentifier)
        // Push again no earlier than 15 minutes from now
        request.earliestBeginDate = Date(timeIntervalSinceNow: 10 * 60)
        do {
           try BGTaskScheduler.shared.submit(request)
            Logger.debug("Push scheduled")
        } catch {
            Logger.error("Could not schedule push: \(error.localizedDescription)")
        }
    }
    
    @available(iOS 13.0, *)
    private func handlePushInteractions(_ task: BGTask) {
        Logger.debug("Processing scheduled push!")
        // Schedule a new refresh task
        
        autoStart()
        task.expirationHandler = {}
        
        let _ = Timer.scheduledTimer(withTimeInterval: 2, repeats: false) {
            timer in
            task.setTaskCompleted(success: true)
            self.schedulePushInteractions()
        }
        
        self.schedulePushInteractions()
    }
    
    public func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        autoStart()
        completionHandler(.newData)
    }
    
    public func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        autoStart()
        return true
    }

    public func applicationWillResignActive(_ application: UIApplication) {
        autoStart()
    }

    public func applicationDidEnterBackground(_ application: UIApplication) {
        autoStart()
        if #available(iOS 13.0, *) {
            self.schedulePushInteractions()
        } else {
            // Fallback on earlier versions
        }
    }

    public func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    public func applicationDidBecomeActive(_ application: UIApplication) {
        _ = NotificationManager.shared.getStatus()
        autoStart()
    }

    public func applicationWillTerminate(_ application: UIApplication) {
        autoStart()
        StorageManager.shared.saveContext()
    }
    
    private func updatePushId(token: String) {
        StorageManager.shared.setPushId(token)
        guard let idDevice = StorageManager.shared.getIdentifierDevice() else { return }
        
        APIManager.handshakeNewDevice { result in
            switch result {
            case .success(let response):
                SessionHelper.shared.storeToken(response.token)
                APIManager.updateDeviceInfo(id: Int64(idDevice), pushID: token) { success in
                    // TODO: Check if we need to log case it fails
                    Logger.debug("\(success)")
                }
            case .failure:
                // TODO: Check if we need to log the error
                Logger.error("error happened on handshake new device")
                break
            }
        }
    }
    
    //***************************
    // NOTIFICATIONS
    //***************************
    
    public func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // 1. Convert device token to string
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        let token = tokenParts.joined()
        // 2. Print device token to use for PNs payloads
        Logger.info("Device Token: \(token)")
        if let oldPushId = StorageManager.shared.getPushId(){
            if oldPushId != token {
                updatePushId(token: token)
            }
        }else{
            updatePushId(token: token)
        }
    }
    
    public func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        self.handleRemoteContent(userInfo)
    }
    
    public func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        autoStart()
        self.handleRemoteContent(userInfo)
        completionHandler(.newData)
    }
    
    private func handleRemoteContent(_ userInfo: [AnyHashable : Any]){
        autoStart()
        
        Logger.debug("Received push notification: \(userInfo)")
        
        if let d = userInfo["data"] as? [String: Any]{
            //avoid updating status
            
            PushNotificationData.saveNotificationData(d)
            
            if let status = d["status"] as? Int{
                StorageManager.shared.setStatusUser(status)
            }
            
            if let warningLevel = d["warning_level"] as? Int{
                StorageManager.shared.setWarningLevel(warningLevel)
            }
            
            NotificationCenter.default.post(name: NSNotification.Name(Constants.Notification.patientChangeStatus), object: nil)
            
            if let title = d["title"] as? String{
                let subtitle = d["subtitle"] as? String
                if let message = d["message"] as? String{
                    NotificationManager.shared.showLocalNotification(title, subtitle: subtitle, message: message)
                }
            }
        }
    }
    
    private func startup(){
        
        _ = NotificationManager.shared.getStatus()
        if StorageManager.shared.getIdentifierDevice() == nil {
            Logger.debug("Device not yet registered")
        }else{
            Logger.debug("MY ID: \(String(describing: StorageManager.shared.getIdentifierDevice()))")
            Logger.debug("TOKN JWT: \( String(describing: StorageManager.shared.getTokenJWT()))")
            Logger.debug("Token ID \( String(describing: StorageManager.shared.getPushId()))")
        }
        
        NotificationManager.shared.getAuthorizationStatus({
            status in
            if status == .allowed{
                DispatchQueue.main.async{
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        })
        
        UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalMinimum)
    }
    
}

extension CoronavirusOutbreakControl { // Original View Controller

    public func handshake(on viewController: UIViewController) {
        
        startup() // todo: look for a better place for this
        
        if StorageManager.shared.getIdentifierDevice() == nil {
            recaptcha?.configureWebView { webview in
                webview.frame = viewController.view.bounds
            }
            
            recaptcha?.validate(on: viewController.view) { [weak self] (result: ReCaptchaResult) in
                do {
                    let googleToken = try result.dematerialize()
                    
                    APIManager.handshakeNewDevice(googleToken: googleToken) { [weak self] result in
                        switch result {
                        case .success(let response):
                            if let deviceID = response.id {
                                SessionHelper.shared.storeToken(response.token)
                                StorageManager.shared.setIdentifierDevice(Int(deviceID))
                                viewController.dismiss(animated: true, completion: nil)
                            } else {
                                self?.renderGoogleValidationError(comment: "Error response code from api", viewController: viewController)
                            }
                        case .failure:
                            self?.renderGoogleValidationError(comment: "Error response code from api", viewController: viewController)
                        }
                    }
                } catch let error {
                    Logger.error("ERROR ON REGISTRATION \(error)")
                    self?.renderGoogleValidationError(comment: "Google validation not passed", viewController: viewController)
                }
            }
        }
    }
    
    private func renderGoogleValidationError(comment: String, viewController: UIViewController) {
        self.recaptcha = try? ReCaptcha(
            apiKey: Constants.Setup.keySite,
            baseURL: URL(string: Constants.Setup.keyDomain)!
        )
    }
    
}

extension CoronavirusOutbreakControl {
    
    //****************************************
    // PERMISSIONS
    //****************************************
    
    public enum CoronavirusOutbreakControlPermission {
        case bluetooth
        case location
        case notification
    }
    
    public func requestPermission(requests: [CoronavirusOutbreakControlPermission] = [.bluetooth, .location, .notification]) {
        for item in requests {
            switch item {
                case .bluetooth:
                    BluetoothManager.shared.askUserPermission()
                case .location:
                    LocationManager.shared.requestPermission()
                case .notification:
                    NotificationManager.shared.requestPermission { (_) in }
                }
        }
    }
    
    public func permissionIsGranted(permission: CoronavirusOutbreakControlPermission) -> Bool {
        switch permission {
            case .bluetooth:
                return BluetoothManager.shared.getPermissionStatus() == .allowed
            case .location:
                return LocationManager.shared.getPermissionStatus() == .allowedAlways
            case .notification:
                return NotificationManager.shared.getStatus() == .allowed
            }
    }

}

extension CoronavirusOutbreakControl {
    
    //****************************************
    // Getters
    //****************************************

    public func getIdentifierDevice() -> Int? {
        return StorageManager.shared.getIdentifierDevice()
    }

}

extension CoronavirusOutbreakControl {
    
    //****************************************
    // Private SDK Controls
    //****************************************
    
    private func autoStart() {
        guard StorageManager.shared.canAutoStart else { return }
        start()
    }
}


extension CoronavirusOutbreakControl {

    //****************************************
    // PUBLIC SDK Controls
    //****************************************
    
    public var canRun: Bool {
        return Utils.isAbleToStart()
    }
    
    public var isRunning: Bool {
        return Utils.isAbleToStart() && IBeaconManager.shared.isRunning && LocationManager.shared.isRunning
    }
    
    public func start() {
        BackgroundManager.backgroundOperations()
    }
    
    public func stop() {
        BackgroundManager.stopBackgroundOperations()
    }
    
    public func setAutoStart(enabled: Bool) {
        StorageManager.shared.setCanAutoStart(enabled)
    }
    
    public var isAutoStartEnabled: Bool {
        return StorageManager.shared.canAutoStart
    }
    
    public func setShareLocation(_ share : Bool) {
        StorageManager.shared.setShareLocation(share)
    }
    
    public func getShareLocation() -> Bool {
        return StorageManager.shared.getShareLocation()
    }
    
    public func getNotification() -> PushNotificationData?{
        return PushNotificationData.readNotificationData()
    }
    
}

extension CoronavirusOutbreakControl{
    
    //******************************************
    // PUBLIC SDK Controls for Permission Status
    //******************************************
    
    public func getBluetoothStatus() -> BluetoothManager.Status {
        return BluetoothManager.shared.getBluetoothStatus()
    }
    
    public func getBluetoothPermissionStatus() -> BluetoothManager.PermissionStatus {
        return BluetoothManager.shared.getPermissionStatus()
    }
    
    public func getBluetoothPermissionChangeEvent() -> String{
        return Constants.Notification.bluetoothChangeStatus
    }
    
    public func getLocationPermissionStatus() -> LocationManager.AuthorizationStatus {
        return LocationManager.shared.getPermissionStatus()
    }
    
    public func getLocationServiceEnabledForAppStatus() -> Bool {
        return LocationManager.shared.isServiceEnabledForApp()
    }
    
    public func getLocationPermissionChangeEvent() -> String{
        return Constants.Notification.locationChangeStatus
    }

    public func getNotificationPermissionStatus() -> NotificationManager.PermissionStatus {
        return NotificationManager.shared.getStatus()
    }
    
    public func getNotificationPermissionChangeEvent() -> String{
        return Constants.Notification.notificationChangeStatus
    }

}


extension CoronavirusOutbreakControl {
    
    //******************************************
    // App settings
    //******************************************

    public func openSettings() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                Logger.debug("Settings opened: \(success)") // Prints true
            })
        }
    }
}
